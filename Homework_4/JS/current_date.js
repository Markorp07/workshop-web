var today = new Date();
var day = new Array(7);
day[0] = "Domingo";
day[1] = "Lunes";
day[2] = "Martes";
day[3] = "Miércoles";
day[4] = "Jueves";
day[5] = "Viernes";
day[6] = "Sábado";
var current_day = day[today.getDay()];

console.log("Hoy es: " + current_day);

var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
console.log("La hora actual es:" + time);